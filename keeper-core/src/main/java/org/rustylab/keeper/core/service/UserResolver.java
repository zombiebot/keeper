package org.rustylab.keeper.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.rustylab.keeper.core.dao.UserDao;
import org.rustylab.keeper.core.entity.User;
import org.rustylab.keeper.core.security.AuthenticationFacade;

@Service("userResolver")
public class UserResolver {

    @Autowired
    private AuthenticationFacade authenticationFacade;

    @Autowired
    private UserDao userDao;

    public User currentUser() {
        String username = authenticationFacade.getAuthentication().getName();
        User user = userDao.findByUserName(username);
        if (user != null) {
            return user;
        }
        return createNewUser(username);
    }

    /** In case of successful auth0 login new user may need to be created. */
    private User createNewUser(String username) {
        User user = new User();
        user.setUsername(username);
        long id = userDao.create(user);
        user.setId(id);
        return user;
    }
}
