package org.rustylab.keeper.core.config;

import static org.apache.commons.io.FilenameUtils.concat;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Responsible for resolving external configuration properties.
 */
public class KeeperConfig {

    // OAuth providers keys
    private static final String FACEBOOK_CLIENT_ID = "facebookClientId";
    private static final String FACEBOOK_CLIENT_SECRET = "facebookClientSecret";
    private static final String GITLAB_CLIENT_ID = "gitlabClientId";
    private static final String GITLAB_CLIENT_SECRET = "gitlabClientSecret";

    // OAuth providers values
    private static String facebookClientId;
    private static String facebookClientSecret;
    private static String gitlabClientId;
    private static String gitlabClientSecret;

    static {
        String configFile = concat(System.getProperty("user.home"), ".keeper/config/config.properties");
        Properties prop = new Properties();
        try (InputStream input = new FileInputStream(configFile)) {
            prop.load(input);
            facebookClientId = prop.getProperty(FACEBOOK_CLIENT_ID);
            facebookClientSecret = prop.getProperty(FACEBOOK_CLIENT_SECRET);
            gitlabClientId = prop.getProperty(GITLAB_CLIENT_ID);
            gitlabClientSecret = prop.getProperty(GITLAB_CLIENT_SECRET);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getFacebookClientId() {
        return facebookClientId;
    }

    public static String getFacebookClientSecret() {
        return facebookClientSecret;
    }

    public static String getGitlabClientId() {
        return gitlabClientId;
    }

    public static String getGitlabClientSecret() {
        return gitlabClientSecret;
    }

}
