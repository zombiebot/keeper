package org.rustylab.keeper.core.dao;

import org.rustylab.keeper.core.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

/**
 * Manages user objects at database level.
 */
@Repository("userDao")
public class UserDao {

    private static final String CREATE_SQL = "insert into User(username) "
            + "values (:username)";

    private static final String FIND_USER_QUERY = "select * from User ";

    @Autowired
    private Sql2o sql2o;

    public User findByUserName(String username) {
        final String sql = FIND_USER_QUERY + " where username = :username";
        try (Connection con = sql2o.open()) {
            return con.createQuery(sql)
                .addParameter("username", username)
                .executeAndFetchFirst(User.class);
        }
    }

    public User findById(Long id) {
        final String sql = FIND_USER_QUERY + " where id = :id";
        try (Connection con = sql2o.open()) {
            return con.createQuery(sql)
                .addParameter("id", id)
                .executeAndFetchFirst(User.class);
        }
    }

    public long create(User user) {
        try (Connection con = sql2o.open()) {
            return con.createQuery(CREATE_SQL, true)
                .addParameter("username", user.getUsername())
                .executeUpdate().getKey(long.class);
        }
    }

}
