package org.rustylab.keeper.core.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.rustylab.keeper.core.dao.NoteDao;
import org.rustylab.keeper.core.dto.NoteDetails;
import org.rustylab.keeper.core.dto.NoteSearchCriteria;
import org.rustylab.keeper.core.entity.Note;
import org.rustylab.keeper.core.validator.NoteValidator;

@Transactional
@Service("noteService")
public class NoteService {

    @Autowired
    private NoteValidator noteValidator;

    @Autowired
    private NoteMapper noteMapper;

    @Autowired
    private NoteDao noteDao;

    @Autowired
    private UserResolver userResolver;

    public NoteDetails saveNote(NoteDetails noteDetails) {
        Note note = noteMapper.dtoToEntity(noteDetails);
        noteValidator.validate(note);
        if (note.getId() != null) {
            note.setUpdated(new Date());
            noteDao.update(note);
        } else {
            note.setCreated(new Date());
            note.setAuthor(userResolver.currentUser());
            noteDao.create(note);
        }
        return noteMapper.entityToDto(note);
    }

    @Transactional(readOnly = true)
    public List<NoteDetails> returnAllNotes() {
        return noteDao.find(null);
    }

    @Transactional(readOnly = true)
    public NoteDetails findNote(long noteId) {
        NoteSearchCriteria criteria = new NoteSearchCriteria();
        criteria.setId(noteId);

        List<NoteDetails> notes = noteDao.find(criteria);
        if (notes.isEmpty()) {
            return null;
        }
        return notes.get(0);
    }

}