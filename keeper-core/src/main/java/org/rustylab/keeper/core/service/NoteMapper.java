package org.rustylab.keeper.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.rustylab.keeper.core.dao.NoteDao;
import org.rustylab.keeper.core.dto.NoteDetails;
import org.rustylab.keeper.core.entity.Note;

/**
 * Default implementation of {@link NoteMapper}.
 */
@Component("noteMapper")
public class NoteMapper {

    @Autowired
    private NoteDao noteDao;

    public NoteDetails entityToDto(Note note) {
        NoteDetails noteDetails = new NoteDetails();
        noteDetails.setId(note.getId());
        noteDetails.setTitle(note.getTitle());
        noteDetails.setText(note.getText());
        return noteDetails;
    }

    public Note dtoToEntity(NoteDetails noteDetails) {
        Note note;
        if (noteDetails.getId() != null) {
            note = noteDao.find(noteDetails.getId());
        } else {
            note = new Note();
        }
        
        note.setTitle(noteDetails.getTitle());
        note.setText(noteDetails.getText());
        return note;
    }

}
