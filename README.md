# keeper
[![Build status](https://ci.appveyor.com/api/projects/status/nesrbxghvulphqf3?svg=true)](https://ci.appveyor.com/project/xeranas/keeper-441bs)

## Build keeper application jar
  - Simple build using maven ```mvn clean install```. Main application jar should be in keeper-app/target directory

## Database preparation
  - Generate initial DB file by running ```mvn flyway:migrate``` from keeper-db directory;
  - Manually insert login user ```insert into User (id, username, password) VALUES(1, 'user', 'encoded_password')``` password should be encoded via BCryptPasswordEncoder.

## Deployment on Unix
  - Start process with nohup ```nohup java -jar keeper-app.jar &``` in order to keep alive after terminal will be closed.

## Troubleshoot
  - If application started but login page aren't opening it could be related to lack of entropy [see details](https://www.digitalocean.com/community/tutorials/how-to-setup-additional-entropy-for-cloud-servers-using-haveged).
