create table User (
    id IDENTITY,
    username varchar(60),
    password varchar(240)
);

create table Note (
    id IDENTITY,
    title varchar(150),
    text CLOB,
    author BIGINT,
    created TIMESTAMP,
    updated TIMESTAMP,
);
