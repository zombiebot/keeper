package org.rustylab.keeper.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

import org.rustylab.keeper.core.security.SecurityConfig;

/**
 * Spring bootstrap application entry point.
 * 
 * @author Arturas Norkus
 * 
 * @since 0.1
 */
@SpringBootApplication
@ImportResource(locations = { "classpath:keeper-db-beans.xml", "classpath:keeper-core-beans.xml" })
@ComponentScan(basePackages = "org.rustylab.keeper")
@Import({ SecurityConfig.class })
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
