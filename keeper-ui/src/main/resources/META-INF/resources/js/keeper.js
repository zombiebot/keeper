if (typeof keeper === 'undefined') {
	keeper = {};
}

keeper.applyLargeModalStyle = function() {
	$(".keeper-modal-lg .modal-dialog").addClass("modal-lg");
}

keeper.setFocusOnEditor = function() {
	$("#editNoteForm ")
}

keeper.filterNotes = function() {
	var searchText = $(".search-text").val();
	
	var notes = $("#notesList .kp-note-result");
	$.each( notes, function( key, value ) {
		if ($(value).text().toLowerCase().indexOf(searchText.toLowerCase()) === -1) {
			$(value).hide();
		} else {
			$(value).show();
		}
	});
}

