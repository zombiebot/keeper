
(function($) {
	// extend jQuery --------------------------------------------------------------------
	$.fn.markdownReadonly = function() {
		var root = $(this);

		var $readonlyMarkdown = root.find('.butter-component-value-readonly-wrapper');
		var markdownText = $readonlyMarkdown.html().replace('&gt;', '>');

		var converter = new showdown.Converter();
		showdown.setOption('simpleLineBreaks', 'true');
		showdown.setOption('openLinksInNewWindow', 'true');
		var markdownTextToHtml = converter.makeHtml(markdownText);
		
		$readonlyMarkdown.empty();
		$readonlyMarkdown.append(markdownTextToHtml);
	};
}(jQuery));